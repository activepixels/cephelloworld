# CEPHelloWorld #

First try at a CEP panel for After Effects 

Follow my forum support discussion at https://forums.adobe.com/message/9806662

## Setup ##

From https://github.com/Adobe-CEP/CEP-Resources/wiki/CEP-6-HTML-Extension-Cookbook-for-CC-2015#debugging-unsigned-extensions

Mac: In the terminal, type: 

`defaults write com.adobe.CSXS.6 PlayerDebugMode 1`

or

`defaults write com.adobe.CSXS.7 PlayerDebugMode 1`

These entries will enable debug extensions to be displayed in the host applications.

## Current Status - fails ##

The panel is visible in Windows > Extensions. It loads fine and displays the initial alert();
 
This block

```javascript
var csInterface = new CSInterface();
   csInterface.evalScript('app.project.file.displayName', function (result) {
   alert(result);
  });
```

returns "EvalScript error."
 
Using the debug console via a browser on the debug port shows no message.
 
I'm aware that app.project is null when project has not been saved, so I launch the panel after a dummy project is loaded.
 
I'm using After Effects 2017.2, CSInterface - v6.1.0 and my core.jsx is empty
 
I've followed the guide to allow unsigned panels to be launched in CC apps

I've also verified that `alert(app.project.file.displayName);` in the extendscript toolkit editor window correctly returns the file name of the current project. I then launched the hello world panel again and saw the same "EvalScript error."